package com.romirom1.crud.model;


/**
 * Created by hakiki95 on 4/16/2017.
 */

public class DataModel {
    String NIK, NAMA, Alamat, Jenis_Pendidikan, Id_Jabatan, Status;

    public String getNIK() {
        return NIK;
    }

    public void setNIK(String NIK) {
        this.NIK = NIK;
    }

    public String getNAMA() {
        return NAMA;
    }

    public void setNAMA(String NAMA) {
        this.NAMA = NAMA;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String alamat) {
        Alamat = alamat;
    }

    public String getJenis_Pendidikan() {
        return Jenis_Pendidikan;
    }

    public void setJenis_Pendidikan(String jenis_Pendidikan) {
        Jenis_Pendidikan = jenis_Pendidikan;
    }

    public String getId_Jabatan() {
        return Id_Jabatan;
    }

    public void setId_Jabatan(String id_Jabatan) {
        Id_Jabatan = id_Jabatan;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
