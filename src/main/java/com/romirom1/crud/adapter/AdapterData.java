package com.romirom1.crud.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.romirom1.crud.MainActivity;
import com.romirom1.crud.R;
import com.romirom1.crud.model.DataModel;

import java.util.List;

public class AdapterData extends RecyclerView.Adapter<AdapterData.HolderData> {

    private List<DataModel> mList ;
    private Context ctx;


    public  AdapterData (Context ctx, List<DataModel> mList)
    {
        this.ctx = ctx;
        this.mList = mList;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist,parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        DataModel dm = mList.get(position);
        holder.nik.setText(dm.getNIK());
        holder.nama.setText(dm.getNAMA());
        holder.alamat.setText(dm.getAlamat());
        holder.dm = dm;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    class HolderData extends  RecyclerView.ViewHolder{
        TextView nik, alamat, nama;
        DataModel dm;
        public HolderData (View v)
        {
            super(v);

            nik = (TextView) v.findViewById(R.id.tvNIK);
            nama = (TextView) v.findViewById(R.id.tvNama);
            alamat = (TextView) v.findViewById(R.id.tvAlamat);
//
//            v.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent goInput = new Intent(ctx,MainActivity.class);
//                    goInput.putExtra("id", dm.getId());
//                    goInput.putExtra("nik", dm.getNama());
//                    goInput.putExtra("nama", dm.getUsia());
//                    goInput.putExtra("alamat", dm.getDomisili());
//
//                    ctx.startActivity(goInput);
//                }
//            });
        }
    }
}
